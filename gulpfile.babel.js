import gulp from 'gulp';
import yaml from 'gulp-yaml';
import rename from 'gulp-rename';
import merge from 'gulp-merge-json';
import babel from 'gulp-babel';
import copy from 'gulp-copy';
import del from 'del';

const destination = 'dist'

gulp.task('clean', function() {
  return del(destination)
})

gulp.task('generation-status', function() {
  return gulp
    .src('./data/statuses/*.yml')
    .pipe(yaml({ space: 2 }))
    .pipe(rename(function (path) {
      // Rename <status>.yml to <status>/tree.json
      path.dirname += '/' + path.basename;
      path.basename = 'tree';
    }))
    .pipe(gulp.dest('./src/statuses/'));
})

gulp.task('generation-taxes', function() {
  return gulp
    .src('./data/taxes/*.yml')
    .pipe(yaml({ space: 2 }))
    .pipe(merge({
      fileName: 'taxes.json',
      edit: function(json) {
        // Exclude special keys form the output (__defs__)
        ['__defs__'].forEach(exclude => json[exclude] = undefined);
        return json;
      }
    }))
    .pipe(gulp.dest('./src/taxes/'));
});

gulp.task('generation', ['generation-status', 'generation-taxes'])

gulp.task('compilation', [ 'clean', 'generation' ], function () {
  const es6 = gulp
    .src(['src/**/*.js', '!src/**/tests/*'])
    .pipe(babel())
    .pipe(gulp.dest(destination));

  const json = gulp
    .src('src/**/*.json')
    .pipe(gulp.dest(destination));

  return [ es6, json ];
});


gulp.task('default', [ 'compilation' ]);
