// import { asTree } from 'treeify';
// import { exercice } from './exercice';

const exercice = require('fisca.js').exercice;
const treeify = require('treeify');

const tree = obj => treeify.asTree(obj, true);

const revenues = [{
  label: 'clients',
  amount: 100000
}]

const costs = [{
  label: 'consommables',
  amount: 5000
}, {
  label: 'frais de bouche',
  amount: 5000
}]

const employees = [{
  label: 'moi',
  gross_monthly_salary: 3750,
  cadre: true
}]

const structure = {
  label: 'mon entreprise',
  status: 'sasu',
  revenues: [],
  costs: [],
  employees: []
};

console.log(" ====== STRUCTURE ====== ");
console.log(tree(structure));

const result = exercice(structure);

console.log(" ====== RESULT ====== ");
console.log(tree(result));
