// @flow

// This defines the behavior of a regressive tax.
// Each bracket, with a floor and a ceiling, defines a specific rate to apply.
// The tax is calculated by applying the rate of each bracket to the
// corresponding slice in the income.

/*::

import type {
  Percent,
  Amount,
  Income,
  Structure,
  Benefits,
  Transaction
} from '../types'

import type {
  StaticTaxDefinition,
} from './index'

export type BracketPreDef = {
  floor: Amount,
  rate: Percent,
}

type BracketDef = {
  floor: Amount,
  ceil: Amount,
  rate: Percent,
}

type Brackets = Array<Bracket>

*/

class Bracket {
  floor/*: Amount */
  ceil/*: Amount */
  rate/*: Percent */

  constructor({ floor, rate, ceil }/*: BracketDef */) {
    this.floor = floor;
    this.rate = rate;
    this.ceil = ceil;
  }

  compute(income) {

    const taxable = Math.max(Math.min(income - this.floor, this.ceil), 0);
    const amount = taxable * this.rate;
    const outcome = income - amount;

    return { amount, outcome }
  }

}

const defaultBracket = () => new Bracket({ floor: 0, ceil: Infinity, rate: 1 });

function fillCeil(brackets/*: Array<BracketPreDef> */)/*: Array<BracketDef>*/ {
  // From the biggest bracket, cascade the floor of a superior bracket
  // into the ceil of the next inferior.

  return brackets
    .sort((a, b) => b.floor - a.floor)
    .reduce((brackets, inferior) => {
      const superior = brackets[0];
      return [ {
        ...inferior,
        ceil: superior ? superior.floor : Infinity
      }, ...brackets ];
    }, []);
}


function mergeBrackets(a, b) {

  const result = [];

  for (
    let ba = a.shift(),
        bb = b.shift(),
        floor = Math.min(ba.floor, bb.floor);
        ;
  ) {

    const ceil = Math.min(ba.ceil, bb.ceil);

    result.push({
      floor,
      ceil,
      rate: ba.rate * bb.rate
    })

    floor = ceil;

    if (a.length === 0 && b.length === 0)
      break;

    if (ba.ceil === ceil)
      ba = a.shift() || defaultBracket();
    if (bb.ceil === ceil)
      bb = b.shift() || defaultBracket();
  }

  return result;
}


export default class Tax {
  brackets/*: Brackets */
  benefits/*: Benefits */

  constructor(structure/*: StaticTaxDefinition*/) {
    const {
      brackets = [defaultBracket()],
      base = [defaultBracket()],
      benefits
    } = structure;

    this.brackets = mergeBrackets(
      fillCeil(brackets),
      fillCeil(base)
    ).map(bracket => new Bracket(bracket));

    this.benefits = benefits;
  }


  compute(income/*: Income */)/*: Transaction */ {
    // Calculate the tax for each bracket of income,
    // and accumulate these taxes, while reducing the income accordingly.
    const { amount } = this.brackets.reduce((result, bracket) => {
      const { outcome, amount } = bracket.compute(result.outcome);

      return {
        // taxes: [
        //   ...result.taxes,
        //   { amount, bracket }
        // ],
        outcome,
        amount: result.amount + amount,
      }
    }, {
      // taxes: [],
      outcome: income,
      amount: 0
    })

    return {
      amount,
      benefits: this.benefits,
    }
  }
}
