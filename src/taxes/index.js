// @flow
import json from './taxes.json';
import Tax from './tax';

/*::

import type {
  Benefits
} from '../types'

import type {
  BracketPreDef
} from './tax'

export type StaticTaxDefinition = {
  label: string,
  base: Array<BracketPreDef>,
  brackets: Array<BracketPreDef>,
  benefits: Benefits
}

type TaxGroup = {
  [taxName: string]: Tax
}

type FormattedTaxes = {
  [groupName: string]: TaxGroup
}

type TaxDefinition = {
  [year: string]: StaticTaxDefinition
}

type RawTaxes = {
  [groupName: string]: TaxDefinition
}

*/

function formatLatestTax(structure/*: TaxDefinition */) /*: Tax */ {
  const latestKey = Object.keys(structure)
    .reduce((latestKey, key) =>
      key > latestKey ? key : latestKey, '0'
    );

  return new Tax(structure[latestKey]);
}

function formatTaxes(taxes/*: TaxGroup */)/*: FormattedTaxes */ {
  return {
    ...Object.entries(taxes)
      .reduce((groupes, [name, group]) => ({
        ...groupes,
        [name]: Object.entries(group)
          .reduce((taxes, [name, tax]) => ({
            ...taxes,
            // $ExpectError flow fail to type result of Object.entries
            [name]: formatLatestTax(tax)
          }), {})
      }), {})
  }
}

export default formatTaxes(json);
