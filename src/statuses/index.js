// @flow
/*::
import type { Statuses } from '../types'
 */
import sasu, { helpers as sasuHelpers } from './sasu';
import me, { helpers as meHelpers } from './me';
import household, { helpers as householdHelpers } from './household';

export const statusList = [{
  id: 'me',
  label: 'ME',
  helpers: meHelpers,
}, {
  id: 'sasu',
  label: 'SASU',
  helpers: sasuHelpers,
}]

const statuses/*: Statuses */ = { sasu, me, household };

export default statuses;
