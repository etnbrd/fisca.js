// @flow
import taxes from '../../taxes';
import Outcomes from './tree.json';

/*::
import type {
  Income,
  InjectOutcomeMap,
  InjectGroupMap,
  ComputeOutcomeMap
} from '../../types'

type DividendDistribution = {
  to_dividend: number,
  to_liquid: number,
}
*/

function RatioDividendDistribution(
  ratio /*: number */
)/* (:Income) => DividendDistribution */ {
  return (income/*: Income */) => ({
    to_dividend: income * ratio,
    to_liquid: income * (1 - ratio)
  });
}

const defaults = {
  dividend_distribution: RatioDividendDistribution(1),
  ratio_salarie_retraite_compl_cadre_trC: .1
};

const {
  groupe_impot_revenu,
  groupe_impot_societes,
  groupe_charges_salariales,
  groupe_charges_patronales,
  groupe_dividende
} = taxes;


const injectOutcome /*: InjectOutcomeMap */ = {
  default: (structure, outcome) => Outcomes[outcome.id]
}


const injectGroup /*: InjectGroupMap */ = {

  external_expenses: (structure, outcome) =>
    structure.expenses.map(expense => ({
      id: 'expense',
      type: 'expense',
      ...expense
    })),

  salaries: (structure, outcome) =>
    structure.employees.map(employee => ({
      ...Outcomes.salary,
      context: { employee }
    })),

  default: (structure, outcome) =>
    // $ExpectError TODO
    outcome.group.map(injectOutcome.default.bind(null, structure))

}


const computeOutcome /*: ComputeOutcomeMap */ = {

  // $ExpectError employee is expected in this context
  net_salary: (structure, income, outcome, { employee }) =>
    ({ employee_id: employee.id, amount: income }),

  // $ExpectError employee is expected in this context
  salary: (structure, income, outcome, { employee }) =>
    ({ employee_id: employee.id, amount: employee.gross_monthly_salary * 12 }),

  impot_societes: (structure, income) =>
    groupe_impot_societes.impot_societes.compute(income),

  dividend_tax: (structure, income) =>
    groupe_dividende.dividende_sasu.compute(income),

  // $ExpectError distribute_dividend is expected or defaulted
  benefit_net: ({ distribute_dividend = defaults.dividend_distribution }, income) =>
    ({ amount: distribute_dividend(income).to_dividend }),

  charges_salariales_csg: (structure, income) =>
    groupe_charges_salariales.csg.compute(income),

  charges_salariales_crds: (structure, income) =>
    groupe_charges_salariales.crds.compute(income),

  charges_salariales_assurance_maladie: (structure, income) =>
    groupe_charges_salariales.assurance_maladie.compute(income),

  charges_salariales_assurance_vieillesse_plafond: (structure, income) =>
    groupe_charges_salariales.assurance_vieillesse_plafond.compute(income),

  charges_salariales_assurance_vieillesse: (structure, income) =>
    groupe_charges_salariales.assurance_vieillesse.compute(income),

  charges_salariales_assurance_chomage: (structure, income) =>
    groupe_charges_salariales.assurance_chomage.compute(income),

  // $ExpectError employee is expected in this context
  charges_salariales_retraite_compl_non_cadre_trA: (structure, income, outcome, { employee }) => {
    if (!employee.cadre) {
      return groupe_charges_salariales.retraite_compl_non_cadre_trA.compute(income);
    }
    return { amount: 0 };
  },

  // $ExpectError employee is expected in this context
  charges_salariales_retraite_compl_non_cadre_trB: (structure, income, outcome, { employee }) => {
    if (!employee.cadre) {
      return groupe_charges_salariales.retraite_compl_non_cadre_trB.compute(income);
    }
    return { amount: 0 };
  },

  // $ExpectError employee is expected in this context
  charges_salariales_retraite_compl_cadre_trA: (structure, income, outcome, { employee }) => {
    if (employee.cadre) {
      return groupe_charges_salariales.retraite_compl_cadre_trA.compute(income);
    }
    return { amount: 0 };
  },

  // $ExpectError employee is expected in this context
  charges_salariales_retraite_compl_cadre_trB: (structure, income, outcome, { employee }) => {
    if (employee.cadre) {
      return groupe_charges_salariales.retraite_compl_cadre_trB.compute(income);
    }
    return { amount: 0 };
  },

  // $ExpectError ratio is expected or defaulted, employee is expected in this context
  charges_salariales_retraite_compl_cadre_trC: ({
    ratio_salarie_retraite_compl_cadre_trC = defaults.ratio_salarie_retraite_compl_cadre_trC
  }, income, outcome, { employee }) => {
    if (employee.cadre) {
      const { amount, benefits } = groupe_charges_salariales.retraite_compl_cadre_trC.compute(income);
      return { amount: ratio_salarie_retraite_compl_cadre_trC * amount, benefits };
    }
    return { amount: 0 };
  },

  charges_patronales_assurance_maladie: (structure, income) =>
    groupe_charges_patronales.assurance_maladie.compute(income),

  charges_patronales_assurance_vieillesse_plafond: (structure, income) =>
    groupe_charges_patronales.assurance_vieillesse_plafond.compute(income),

  charges_patronales_assurance_vieillesse: (structure, income) =>
    groupe_charges_patronales.assurance_vieillesse.compute(income),

  charges_patronales_allocations_familiales: (structure, income) =>
    groupe_charges_patronales.allocations_familiales.compute(income),

  charges_patronales_aide_logement: (structure, income) =>
    groupe_charges_patronales.aide_logement.compute(income),

  charges_patronales_assurance_chomage: (structure, income) =>
    groupe_charges_patronales.assurance_chomage.compute(income),

  charges_patronales_fond_garantie_salaires: (structure, income) =>
    groupe_charges_patronales.fond_garantie_salaires.compute(income),

  // $ExpectError employee is expected in this context
  charges_patronales_retraite_compl_non_cadre_trA: (structure, income, outcome, { employee }) => {
    if (!employee.cadre) {
      return groupe_charges_patronales.retraite_compl_non_cadre_trA.compute(income);
    }
    return { amount: 0 };
  },

  // $ExpectError employee is expected in this context
  charges_patronales_retraite_compl_non_cadre_trB: (structure, income, outcome, { employee }) => {
    if (!employee.cadre) {
      return groupe_charges_patronales.retraite_compl_non_cadre_trB.compute(income);
    }
    return { amount: 0 };
  },

  // $ExpectError employee is expected in this context
  charges_patronales_retraite_compl_cadre_trA: (structure, income, outcome, { employee }) => {
    if (employee.cadre) {
      return groupe_charges_patronales.retraite_compl_cadre_trA.compute(income);
    }
    return { amount: 0 };
  },

  // $ExpectError employee is expected in this context
  charges_patronales_retraite_compl_cadre_trB: (structure, income, outcome, { employee }) => {
    if (employee.cadre) {
      return groupe_charges_patronales.retraite_compl_cadre_trB.compute(income);
    }
    return { amount: 0 };
  },

  // $ExpectError employee is expected in this context
  charges_patronales_retraite_compl_cadre_trC: ({
    ratio_salarie_retraite_compl_cadre_trC = defaults.ratio_salarie_retraite_compl_cadre_trC
  }, income, outcome, { employee }) => {
    if (employee.cadre) {
      const { amount, benefits } = groupe_charges_patronales.retraite_compl_cadre_trC.compute(income);
      return { amount: (1 - ratio_salarie_retraite_compl_cadre_trC) * amount , benefits };
    }
      return { amount: 0 };
  },

  charges_patronales_assurace_deces: (structure, income) =>
    groupe_charges_patronales.assurace_deces.compute(income),

  charges_patronales_formation_professionnelle: (structure, income) =>
    groupe_charges_patronales.formation_professionnelle.compute(income),
}

export const helpers = {
  RatioDividendDistribution
};

export default {
  status: 'sasu',
  injectOutcome,
  injectGroup,
  computeOutcome
};
