import { exercice } from 'fisca.js';

function structure_SASU({ income = 100000, expenses = 10000, salary = 3750 }) {
  return {
    label: 'mon entreprise',
    status: 'sasu',
    revenues: [{
      label: 'clients',
      amount: income
    }],
    expenses: [{
      label: 'expenses',
      amount: expenses
    }],
    stakeholders: [{
      label: 'moi',
      gross_monthly_salary: salary,
      cadre: true
    }]
  };

  structure.revenues[0].amount = income
  return structure
}

describe('SASU', function() {

  function testWithParameters(situation) {
    return function() {
      // GIVEN
      const structure = structure_SASU(situation);
      const result = exercice(structure);

      // THEN
      // TODO In lack of valid exercises results from a real situation, we use
      // snapshots
      it('should match the expected flow', function() {
        expect(result).toMatchSnapshot()
      })
    }
  }

  describe('Revenue nulle', testWithParameters({ income: 0 }));
  // Different level of revenues
  describe('Revenue 10K', testWithParameters({ income: 10000 }));
  describe('Revenue 20K', testWithParameters({ income: 20000 }));
  describe('Revenue 30K', testWithParameters({ income: 30000 }));
  describe('Revenue 40K', testWithParameters({ income: 40000 }));
  describe('Revenue 50K', testWithParameters({ income: 50000 }));
  describe('Revenue 60K', testWithParameters({ income: 60000 }));
  describe('Revenue 70K', testWithParameters({ income: 70000 }));
  describe('Revenue 80K', testWithParameters({ income: 80000 }));
  describe('Revenue 90K', testWithParameters({ income: 90000 }));
  // Revenues with plausible values
  describe('Revenue 39345.34', testWithParameters({ income: 39345.34 }));
  describe('Revenue 62995.10', testWithParameters({ income: 62995.10 }));
  describe('Revenue 93094.99', testWithParameters({ income: 93094.99 }));

  // Expenses
  describe('Expenses 1K', testWithParameters({ expenses: 1000 }));
  describe('Expenses 5K', testWithParameters({ expenses: 5000 }));
  describe('Expenses 10K', testWithParameters({ expenses: 10000 }));
  describe('Expenses 30K', testWithParameters({ expenses: 30000 }));

  // Salary
  describe('Salary 1K', testWithParameters({ salary: 1000 }));
  describe('Salary 2K', testWithParameters({ salary: 2000 }));
  describe('Salary 5K', testWithParameters({ salary: 5000 }));
  describe('Salary 10K', testWithParameters({ salary: 10000 }));
});
