// @flow
import taxes from '../../taxes';
import Outcomes from './tree.json';

/*::
import type {
  InjectOutcomeMap,
  InjectGroupMap,
  ComputeOutcomeMap,
} from '../../types'
*/

const {
  groupe_impot_revenus
} = taxes;


const injectOutcome /*: InjectOutcomeMap */ = {

  default: (structure, outcome) => Outcomes[outcome.id]

}


const injectGroup /*: InjectGroupMap */ = {

  default: (structure, outcome) =>
    // $ExpectError TODO
    outcome.group.map(injectOutcome.default.bind(null, structure))

}


const computeOutcome /*: ComputeOutcomeMap*/ = {

  impot_revenues: (structure, income) =>
    // TODO there are huge difference in imposition depending on the type of revenue (salary, freelance or dividends) that are currently not taken into account
    groupe_impot_revenus.impot_revenus.compute(income)

}


export const helpers = {}


export default {
  status: 'household',
  injectOutcome,
  injectGroup,
  computeOutcome
}
