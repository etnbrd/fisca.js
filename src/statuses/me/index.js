// @flow
import taxes from '../../taxes';
import Outcomes from './tree.json';

/*::
import type {
  InjectOutcomeMap,
  InjectGroupMap,
  ComputeOutcomeMap,
} from '../../types'
*/

const {
  groupe_me
} = taxes;


const injectOutcome /*: InjectOutcomeMap */ = {

  default: (structure, outcome) => Outcomes[outcome.id]

}


const injectGroup /*: InjectGroupMap */ = {

  default: (structure, outcome) =>
    // $ExpectError TODO
    outcome.group.map(injectOutcome.default.bind(null, structure))

}


const computeOutcome /*: ComputeOutcomeMap*/ = {

  cfe: (structure, income) => {
    return { amount: 0, benefits: [] } // TODO
  },

  cipav: (structure, income) =>
    groupe_me.cipav.compute(income),

  cfp: (structure, income) =>
    groupe_me.formation_professionnelle.compute(income),
}


export const helpers = {}


export default {
  status: 'me',
  injectOutcome,
  injectGroup,
  computeOutcome
}
