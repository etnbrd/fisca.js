// @flow
import statuses from './statuses'
import { reduce } from './helpers'

/*
  A situation is an abstract way to express a structure.
  It allows to switch quickly between several status.
  In one status a stakeholder might be an employee, whereas in another status,
  it might be the main owner/manager.
*/

/*::
import type {
  Income,
  Situation,
  Structure,
  ConcreteStructures
} from './types'

*/

const structures = {
  sasu: ({ id, label, status, expenses, stakeholders, ...situation }) => ({
    id,
    label,
    status: 'sasu',
    root: 'revenue',
    expenses,
    employees: stakeholders,
    ...statuses.sasu,
  }),

  me: ({ id, label, status, ...situation }) => ({
    id,
    label,
    status: 'me',
    root: 'revenue',
    ...statuses.me,
  }),

  household: ({ id, label, ...situation }) => ({
    id,
    label,
    status: 'household',
    root: 'revenue',
    ...statuses.household,
  })
}

const dependencies = {
  sasu: (situation) => {
    return situation.stakeholders.map(function(stakeholder) {

      function revenues(parentOutcome) {
        const salary = reduce(parentOutcome, [], (agg, outcome) => {
          if (outcome.id === 'net_salary'
          &&  outcome.employee_id === stakeholder.id)
            return [ ...agg, outcome ]
          return agg
        })

        const dividend = reduce(parentOutcome, [], (agg, outcome) => {
          if (outcome.id === 'dividend_net')
            // TODO for simplification, we consider equal splitting of dividend
            // among the stakeholders.
            return [ ...agg, { amount: outcome.amount / situation.stakeholders.length } ]
          return agg
        })

        return [ ... salary, ...dividend ]
      }

      return child.household(revenues, {
        id: stakeholder.id,
        label: stakeholder.label
      })
    })
  },

  me: (structure) => {
    return situation.stakeholders.map(function(stakeholder) {

      function revenues(parentOutcome) {
        const revenue = reduce(parentOutcome, [], (agg, outcome) => {
          if (outcome.type === 'benefit')
            return [ ...agg, outcome ]
          return agg
        })
      }

      return child.household(revenues, {
        id: stakholder.id,
        label: stakholder.label
      })
    })
  }
}

const child = {
  sasu: (revenues, situation) => ({
    revenues,
    structure: structures.sasu(situation),
    children: dependencies.sasu(situation)
  }),

  me: (revenues, situation) => ({
    revenues,
    structure: structures.me(situation),
    children: dependencies.me(situation),
  }),

  household: (revenues, situation) => ({
    revenues,
    structure: structures.household(situation),
    // no children
  })
}

// export function toStructures(
//   situation/*: Situation */
// )/*: ConcreteStructures */ {
//   const { status, stakeholders } = situation
//
//   const revenues /*: Income */ = situation.revenues
//     .reduce((sum, revenue) => sum + revenue.amount, 0);
//
//   const main = structures[status](situation)
//   const households = stakeholders.map(structures.household)
//
//   return [
//     main,
//     ...households
//   ]
// }


/*::

type ChildStructure = {
  revenues: (parent: ConcreteStructure) => Income,
  structure: ConcreteStructure,
  children?: ChildStructures
}

type ChildStructures = Array<ChildStructure>

*/

export function structuresTree(
  situation/*: Situation */
)/*: StructuresTree*/ {
  const { status } = situation
  const revenues = () => situation.revenues
  return child[status](revenues, situation)
}
