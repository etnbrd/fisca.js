import { exercice } from './exercice';
import { statusList } from './statuses';
import { structuresTree } from './situation';
import { simulation } from './simulation';
import parameters from './simulation/parameters';
import outputs from './simulation/outputs';
import * as helpers from './helpers';

export default function compute(
  situation /*: Situation */
)/* StructuresTree */ {

  const tree = structuresTree(situation)

  function iterate(node, parentOutcome) {
    const revenues = node.revenues(parentOutcome)
    const income /*: Income */ = revenues
      .reduce((sum, revenue) => sum + revenue.amount, 0);

    const outcome = exercice(node.structure, income)
    node.outcome = outcome

    if (node.children) {
      node.children.forEach(node => iterate(node, outcome))
    }
  }


  iterate(tree)

  return tree
}

export {
  compute,
  exercice,
  simulation,
  parameters,
  outputs,
  statusList,
  helpers
};
