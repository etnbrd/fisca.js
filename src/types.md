# Model

A model allows to prompt for a given (abstract) situation, and transforms it into a (concrete) composition of structures, allowing to get the outcome of this given situation.

## situation
A situation defines abstract notions to define the structures that will emerge.
Revenues, Expenses, Stakeholders, Status ...
From this abstract situation, it is possible to define one concrete set of structure, a composition.

## composition
A composition defines in legal terms the concretisation of a situation.
It gathers together a set of structures to describe the financial flow throughout the situation.

## structure
A structure is a legal entity that can collect a revenue, pay tax, receive benefits and output an financial outcome.
All structures are defined by a status referring to a financial graph to represent the financial flow from revenue to outcome.

# Simulation

A simulation allows to prompt for parameters and outputs, and concretize (modelize) all the possible situations.

## Parameters
The parameters of a simulation are the Variations and Outputs.

## Variations
A variation represents a set of different values an attribute of the situation can take.
The variation are injected into a base situation by a function `inject`.
It can be a change of a single attribute (status), or a more complex change, like making all the salary for the stakeholder amount to a certain value, while keeping proportionnality.

## Outputs
An output represents the set of values derived from the compositions modeled out of the variations in the initial situation.
Similarly to variations, it is extracted by a function `get`, so it can be derived from a single outcome (benefit), or a more complex one, like the sum of all the net outcomes.
