// @flow
/*::
export type Percent = number
export type Amount = number;
export type Income = Amount;

type Revenue = {|
  id: string,
  label: string,
  amount: Amount,
|}

type Revenues = Array<Revenue>

type Expense = {|
  id: string,
  label: string,
  amount: Amount,
|}

type Expenses = Array<Expense>

type Stakeholder = {|
  id: string,
  label: string,
  salary: Amount,
|}

type Stakeholders = Array<Stakeholder>

export type Situation = {
  status: string,
  revenues: Revenues,
  expenses: Expenses,
  stakeholders: Stakeholders,
}


export type Structure = Situation & {
  root: string,
  income: Income,
  ...Status
}

export type Structures = Array<Structure>

export type ConcreteStructure = Structure & {
  outcome: ConcreteOutcome
}

export type ConcreteStructures = Array<ConcreteStructure>

// TODO Might move to statuses

type InjectOutcome = (Structure, Identifier) => AbstractOutcome
type InjectGroup = (Structure, AbstractParentOutcome) => ContextualizedOutcomeGroup
type ComputeOutcome = (Structure, Income, ?AbstractOutcome, ?Context) => Transaction

type FunctionsMap<F> = {
  [string]: F
}

type FunctionsMapDefault<F> = {
  ...FunctionsMap<F>,
  default: F
}

export type InjectOutcomeMap = FunctionsMapDefault<InjectOutcome>
export type InjectGroupMap = FunctionsMapDefault<InjectGroup>
export type ComputeOutcomeMap = FunctionsMap<ComputeOutcome>

export type Status = {|
  status: string,
  injectOutcome: InjectOutcomeMap,
  injectGroup: InjectGroupMap,
  computeOutcome: ComputeOutcomeMap,
|}

export type Statuses = {
  [string]: Status
}

// TODO might move to an outcome tree dedicated file

type OutcomeId = string
type OutcomeType = "tax" | "salary" | "dividend" | "expense"

export type Identifier = {|
  id: OutcomeId
|}

export type Transaction = {|
  amount: Amount,
  benefits?: Benefits,
|}

type IdentifierGroup = Array<Identifier>

type Outcome = {|
  ...Identifier,
  ...Transaction,
  label: string,
  type: OutcomeType
|}

export type AbstractParentOutcome = {|
  ...Identifier,
  ...Transaction,
  label: string,
  type: "group",
  group: IdentifierGroup,
  output?: Identifier,
|}

export type AbstractOutcome = Outcome | AbstractParentOutcome;

type AbstractOutcomeGroup = Array<AbstractOutcome>

export type ConcreteParentOutcome = {|
  ...Identifier,
  ...Transaction,
  label: string,
  type: "group",
  group: ConcreteOutcomeGroup,
  output?: ConcreteOutcome
|}

export type ConcreteOutcome = Outcome | ConcreteParentOutcome;

type ConcreteOutcomeGroup = Array<ConcreteOutcome>

export type SummedConcreteOutcomeGroup = {|
  sum: Amount,
  group: ConcreteOutcomeGroup
|}

type Benefit = {|
  id: string,
  label: string,
|}

export type Benefits = Array<Benefit>

export type Context = Object

export type ContextualizedOutcome = {|
  ...AbstractOutcome,
  context?: Context
|}

export type ContextualizedOutcomeGroup = Array<ContextualizedOutcome>
*/
