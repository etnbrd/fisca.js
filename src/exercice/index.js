// @flow

/*::

import type {
  Income,
  Amount,
  Structure,
  ConcreteStructure,
  Identifier,
  Transaction,
  AbstractOutcome,
  AbstractParentOutcome,
  ConcreteOutcome,
  Context,
  ContextualizedOutcomeGroup,
  SummedConcreteOutcomeGroup,
} from '../types';

*/


function injectOutcome(
  structure /*: Structure */,
  outcome /*: Identifier */
)/*: AbstractOutcome */ {
  return (
    structure.injectOutcome[outcome.id] ||
    structure.injectOutcome.default
  )(structure, outcome);
}


function injectGroup(
  structure /*: Structure */,
  outcome /*: AbstractParentOutcome */
)/*: ContextualizedOutcomeGroup */ {
  return (
    structure.injectGroup[outcome.id] ||
    structure.injectGroup.default
  )(structure, outcome);
}


function computeOutcome(
  structure /*: Structure */,
  income /*: Income */,
  outcome /*: AbstractOutcome */,
  context /*: Context */ = {}
)/*: Transaction */ {
  if (outcome.amount)
    return { amount: outcome.amount, benefits: [] };

  const compute = structure.computeOutcome[outcome.id];

  return compute
    ? compute(structure, income, outcome, context)
    : { amount: income, benefits: [] }
}


function reduceGroup(
  structure /*: Structure */,
  income /*: Income */,
  outcome /*: AbstractParentOutcome */,
  parentContext /*: Context */ = {}
)/*: SummedConcreteOutcomeGroup */ {
  return injectGroup(structure, outcome)
    .reduce(function({ sum, group }, child) {
      const { context: childContext, ...preOutcome } = child;

      const context = { ...parentContext, ...childContext };
      const outcome = reduceOutcome(structure, income, preOutcome, context);
      return {
        sum: sum + outcome.amount,
        group: [ ...group, outcome ]
      }
    }, {
      sum: 0,
      group: []
    })
}


function reduceOutcome(
  structure /*: Structure */,
  income /*: Income */,
  outcome /*: AbstractOutcome */,
  context /*: Context */ = {},
) /*: ConcreteOutcome */ {
  const { id, type, label } = outcome;
  const {
    amount: available_income,
    benefits,
    ...computedContext
  } = computeOutcome(structure, income, outcome, context);

  // TODO I would like to write the follwing conditional and return only once,
  // but flow doesn't yet support ternary operation to refine the
  // AbstractOutcome into an AbstractParentOutcome.
  // TODO type casting ?
  // const { sum, group } = outcome.type === 'group'
  //   ? reduceGroup(structure, available_income, outcome, context)
  //   : { sum: available_income, group: undefined }
  if (outcome.type === 'group') {
    const { sum, group } = reduceGroup(structure, available_income, outcome, context)

    const output = outcome.output
      ? reduceOutcome(
          structure,
          income - sum,
          injectOutcome(structure, outcome.output),
          context
        )
      : undefined

    const amount = sum + (output ? output.amount : 0);
    return { id, type: outcome.type, label, amount, benefits, group, output, ...computedContext }
  }

  const amount = available_income;
  return { id, type: outcome.type, label, amount: available_income, benefits, ...computedContext }
}


/**
 * exercice
 * Given an abstract structure, it builds the resulting tree of outcomes
 * following the status indicated in the structure.
 */
export function exercice(
  structure /*: ConcreteStructure */,
  income /*: Income */,
) /*: ConcreteOutcome */ {
  return reduceOutcome(
    structure,
    income,
    injectOutcome(
      structure,
      { id: structure.root }
    )
  );
}
