export function reduce(outcome, agg, fn) {
  agg = fn(agg, outcome);

  if (outcome.type === 'group') {
    agg = outcome.group.reduce((agg, outcome) => reduce(outcome, agg, fn), agg)
  }

  if (outcome.output) {
    return reduce(outcome.output, agg, fn);
  }

  return agg;
}

// TODO not very fp
export function walk(outcome, filters, trigger) {
  if (filters.some(filter =>
        filter === '*' ||
        filter === outcome.id)) {
    trigger(outcome);
  }

  if (outcome.type === 'group') {
    outcome.group.forEach(outcome => walk(outcome, filters, trigger))
  }

  if (outcome.output) {
    walk(outcome.output, filters, trigger);
  }
}
