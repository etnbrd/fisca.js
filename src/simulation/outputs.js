import { reduce } from '../helpers';

const structures = {
  sasu: (outcome) =>
    reduce(outcome, 0, function(sum, outcome) {
      if (   outcome.id === 'net_salary'
      ||  outcome.id === 'dividend_net') {
        return sum + outcome.amount
      }

      return sum;
      // return sum +
      //   (   outcome.id === 'net_salary'
      //   ||  outcome.id === 'dividend_net')
      //     ? outcome.amount
      //     : 0
    }),

  me: (outcome) =>
    reduce(outcome, 0, function(sum, outcome) {
      if (outcome.id === 'benefit') {
        return sum + outcome.amount
      }

      return sum;

      // return sum +
      //       (outcome.id === 'benefit')
      //     ? outcome.amount
      //     : 0
    })
}

function finalRevenu(situation, outcome) {
  return structures[situation.status](outcome)
}


export default {
  maximize: [{
    id: 'revenue',
    get: finalRevenu
  }],

  compare: [{
    id: 'revenue',
    label: 'Revenue',
    get: finalRevenu
  }]
};
