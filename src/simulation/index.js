import { exercice } from '../exercice';
import { concretizeParameter } from './parameters'

/*
  The simulation takes a structure, some parameters, and expected output, and
  generate an exercise for every possible situations within the parameters.
  It then produce the expected output out of these situations.
*/




// function simplifyStructure(structure) {
//   return {
//     ...structure,
//     revenues: structure.revenues.reduce((sum, revenue) => sum + revenue.amount, 0),
//     costs: structure.costs.reduce((sum, cost) => sum + cost.amount, 0)
//   }
// }


function* combinationIterator(parameters) {

  // Instead of computing all the parameters combination,
  // this algorithm computes the divisors to transform an iteration index into
  // the indexes of the set of parameters.
  // Also the last divisor computed (the first in the result) is the total number of iterations
  const [ total, ...divisors ] = parameters
    .reduceRight((divisors, { values }) => [
        divisors[0] * values.length,
        ...divisors
      ], [ 1 ]);

  function getPermutation(n) {
    return parameters.reduce((situation, { values, ...parameter }, i) => [
        ...situation,
        {
          ...parameter,
          value: values[Math.floor(n / divisors[i]) % values.length]
        }
      ], []);
  }

  let index = 0;
  while(index < total)
    yield getPermutation(index++);
}


function applyCombination(structure, combination) {
  return combination.reduce((structure, parameter) =>
    parameter.inject(structure),
    JSON.parse(JSON.stringify(structure)));
}


export function simulation(structure, parameters, output) {
  const concreteParameters = parameters.map(concretizeParameter);
  const combinations = Array.from(combinationIterator(concreteParameters));

  const simulations = combinations.reduce((accu, combination) => {
      const situation = applyCombination(structure, combination)
      const result = exercice(situation);

      return [
        ...accu,
        {
          result,
          combination,
          situation,
        }
      ]
    }, [])

  const maximize = output.maximize
    ? output.maximize.map(({ id, get }) => {
        return {
          id,
          ...simulations.reduce((max, { result, situation }) => {
            const value = get(situation, result);
            return value >= max.value
              ? { value, result, situation }
              : max;
          }, {
            value: -Infinity,
          })
        }
      })
    : undefined

  const compare = output.compare
    ? output.compare.map(({ id, get }) => {
        return {
          id,
          simulations: simulations.reduce((accu, { result, situation }) => {
            const value = get(situation, result);
            return [
              ...accu,
              { value, result, situation }
            ]
          }, [])
        }
      })
    : undefined

  return {
    maximize,
    compare
  }
}
