import statuses from '../statuses'

export default [{
  id: 'status',
  label: 'Status',
  inject: function(s) { s.status = this.value; return s },
  type: 'options',
  options: statuses,
},{
  id: 'salary',
  label: 'Salaire',
  inject: function(s) { s.employees[0].gross_monthly_salary = this.value; return s },
  type: 'range'
}, {
  id: 'income',
  label: 'Revenues',
  inject: function(s) { s.revenues[0].amount = this.value; return s },
  type: 'range'
}]


/*
  The concretizeation of parameters is about taking a parameter, and outputing a
  discrete range of value to inject into a given structure.
*/

function generateArray(init, step, length) {
  const arr = Array.apply(null, { length }).map((_, i) => init + (step * i));
  return arr;
}

const concretizeOptions= {
  options(options) {
    const values = options
      .map(option => option.value)
      .filter(option => option);
    return values.length ? values : undefined;
  },
  range(options) {
    const length = 11;
    const step = (options.max - options.min) / (length - 1);
    return generateArray(options.min, step, length)
  }
}

export function concretizeParameter({ id, inject, type, options }) {
  const values = concretizeOptions[type](options);
  return values ? { id, inject, values } : undefined;
}
