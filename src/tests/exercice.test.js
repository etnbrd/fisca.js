import { exercice } from 'fisca.js';
import { structure } from './structure.mock';

describe('exercice', () => {

  // Given a structure
  const result = exercice(structure);

  // The result should match the snapshot
  it('should match snapshot', () => {
    expect(result).toMatchSnapshot();
  })
})
