const revenues = [{
  label: 'clients',
  amount: 100000
}]

const expenses = [{
  label: 'consommables',
  amount: 5000
}, {
  label: 'frais de bouche',
  amount: 5000
}]

const stakeholders = [{
  id: 'moi',
  label: 'Moi',
  gross_monthly_salary: 3750,
  cadre: true
}]

export const situation = {
  label: 'mon entreprise',
  status: 'sasu',
  revenues,
  expenses,
  stakeholders
};
