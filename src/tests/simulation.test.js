import { simulation, helpers, parameters, outputs } from 'fisca.js';
import { structure } from './structure.mock';

describe('simulation', () => {

  const statusParameter = {
    ...parameters.find(param => param.id === 'status'),
    options: [{
      value: 'sasu',
      label: 'sasu'
    }, {
      value: 'me',
      label: 'me'
    }]
  }

  // Given a structure
  const result = simulation(structure, [ statusParameter ], outputs);
  
  // The result should match the snapshot
  it.skip('should match snapshot', () => {
    expect(result).toMatchSnapshot();
  })
})
